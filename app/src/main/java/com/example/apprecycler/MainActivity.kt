package com.example.apprecycler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //setting my list items for my recycler view
        val exampleList = generateDummyList(500)
        //setting the adapter and list that contains my recycle data
        recycler_view.adapter = ExampleAdapter(exampleList)
        //setting the layout orientation for my recycler view
        recycler_view.layoutManager = LinearLayoutManager(this)
        //setting size to data size
        recycler_view.setHasFixedSize(true)
    }

    // function to populate my recycler view
    private fun generateDummyList(size: Int): List<ExampleItem> {
        //ArrayList in android is used to store dynamically sized collection of elements
        val list = ArrayList<ExampleItem>()

       //using a for loop to display my items images
//        for (i in 0 until size) {
//            val drawable = when (i % 3) {
//                0 -> R.drawable.ic_control_point_duplicate_black_24dp
//                1 -> R.drawable.ic_data_usage_black_24dp
//                else -> R.drawable.ic_launcher_background
//            }
//            //setting my items to declare items defined in my live data class ExampleItem
//            val item = ExampleItem(drawable, "Item $i", "Line 2")
//            //loop iterator
//            list += item
//        }

        //or
        //adding manually
        list.add(
            ExampleItem(
            R.drawable.ic_launcher_background,
            "one",
            "two"

        ))

        list.add(
            ExampleItem(
            R.drawable.ic_launcher_foreground,
            "three",
            "four"
        ))

        list.add(ExampleItem(
            R.drawable.ic_data_usage_black_24dp,
            "three",
            "four"
        ))

        //return my recycler view item
        return list
    }
}

